import java.util.Scanner;

public class ArrayRange {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n=sc.nextInt(),max,min;
        int[] arr = new int[n];
        max=Integer.MIN_VALUE;
        min=Integer.MAX_VALUE;
        for(int i=0;i<n;i++) {
            arr[i] = sc.nextInt();
            if(arr[i]>max)
                max=arr[i];
            if(arr[i]<min)
                min=arr[i];

        }
        System.out.println(max-min );
    }
}
